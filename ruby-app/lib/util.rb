
require 'message'
require 'assert'

require 'open3'

# Print a message and exit with an error status.
def die(*msg)
  puts msg.map{|m| m.to_s}.join(" ")
  exit 1
end

# Return a new Struct instance whose fields are the given keys, set to
# the corresponding values.
def new_struct(**args)
  keys, values = args.to_a.transpose
  return Struct.new(*keys).new(*values)
end

def in_tmp(prefix_suffix = nil, &proc)
  Dir.mktmpdir(prefix_suffix) do |tmpdir|
    Dir.chdir(tmpdir, &proc)
  end
end

# Cross-platform way of finding an executable in the $PATH.
#
# https://stackoverflow.com/questions/2108727
#
#   which('ruby') #=> /usr/bin/ruby
def which(cmd)
  exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
  ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each do |ext|
      exe = File.join(path, "#{cmd}#{ext}")
      return exe if File.executable?(exe) && !File.directory?(exe)
    end
  end
  nil
end



# This is the equivalent of `cmd` except:
#
# 1. The command and args is a list, not a string.
# 2. Both stdout and stderr are returned.
# 3. It throws ExternalToolFailed on error.
# 4. It will print messages if enabled.
#

class ExternalToolFailed < RuntimeError; end

def run(cmd)
  cmdstr = cmd.join(' ')
  blabber "RUN: #{cmdstr}"

  output, status = Open3.capture2e(*cmd)
  fmt_out = output.gsub(/^/, '> ')
  blabber "Result:\n#{fmt_out}"

  return output if status.success?

  raise ExternalToolFailed.new(
          "CMD: #{cmd.join(' ')}\nOutput:\n#{fmt_out}"
        )
end


=begin
# Wrap and rethrow an IO error with an IIError or subclass
def iowrap
  begin
    result = yield()
  rescue IOError,SystemCallError => e
    raise IIError, "IO Error: #{e.message}"
  end
  return result
end
=end


# Read in a file and return its contents as a string.  If
# 'allowMissing' is true, return an empty string if the file does not
# exist.
def slurp(filename, allowMissing = false)
  return '' if allowMissing && !File.exist?(filename)
  return iowrap do 
    File.open(filename, "r") { |fh| fh.lines.to_a.join("") }
  end
end

# Write the contents of string 'text' to file at 'filename', creating
# or overwriting the file.
def unslurp(filename, text)
  iowrap do
    File.open(filename, "w") { |fh| fh.write(text) }
  end
  return nil
end


# Buffer for gracefully handling pipe closing in the middle of long
# output.
#
# for handling the case where the user pipes to less and then quits
# partway through.
# 
class PipeCleaner
  def initialize(output)
    @output = output
    @closed = false
  end

  # This gets set by 
  def closed?
    return @closed || @output.closed?
  end
  
  def puts(*args)
    args = [''] if args.empty?
    args.flatten!
    args.each{|line|
      line = "#{line}"
      print("#{line.chomp}\n")
    }
  end

  def print(line)
    return if closed?
    @output.print(line)
  rescue Errno::EPIPE
    @closed = true
    return nil
  end
end

