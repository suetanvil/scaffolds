#!/usr/bin/env ruby

# Placeholder mainline routine

require 'util'
require 'assert'
require 'html_builder'
require 'message'

require 'optparse'


def getopts
  opts = new_struct(
    modern: true,
    punct: '!'
  )

  OptionParser.new do |opo|
    opo.banner = "Usage: main.rb [options]"

    opo.on("-c", "--classic", "Classic mode.") {
      opts.modern = false
    }

    opo.on("-p", "--punct CHAR", "Set alternate punctuation.") { |p|
      opts.punct = p
    }
  end.parse!
  return opts
end

def main
  opts = getopts()

  info html_body("Hi!") {
    text opts.modern ? "Hello, world#{opts.punct}" : "hello world";
  }
end

main

