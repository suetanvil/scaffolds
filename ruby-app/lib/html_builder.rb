
# Low-effort XML/HTML generator.  Main entry point is 'html' and
# 'html_body' but you can use the classes too.


# Inspired by https://gist.github.com/zeitnot/959d5501bb2e15858cca87a3d9db4cb6
# (Mine is 80 lines longer but much cleverer (IMO)).

require 'set'
require 'assert'

# Create an XML document.  Method 'tag' is the entry point; it
# evaluates a block, then creates a tag that wraps the tags or text
# created by that block.
class XmlBuilder
  Node = Struct.new(:tag, :attributes, :children)

  def initialize(root_name, root_attributes = {}, pretty: true)
    @root = make_tag_node(root_name, root_attributes)
    @current = @root
    @pretty = pretty
  end

  def tag(name, **attributes, &kidblock)
    node = make_tag_node(name, attributes)

    addchild(node)
    with_current(node, &kidblock) if kidblock
  end

  def text(str)
    addchild(str)
  end

  def render
    render_node(@root, 0)
  end

  protected

  def end_empty(tag)
    return nl("/>")
  end

  def render_node(node, depth)
    # Case 1: It's a string
    if node.is_a? String
      node = launder(node)

      # Ensure node ends with at least one whitespace character.  If
      # we're pretty-printing, it's going to be a newline.
      node = nl(node) if !node.end_with?("\n")
      node += " " unless node =~ /\s$/

      return node
    end

    # Otherwise, we assume it's a Node
    result = "#{indent(depth)}<#{node.tag}"
    node.attributes.each{|key, value|
      result += " #{key}=\"#{launder(value)}\""
    }

    # Empty tags (i.e. those without children) may and differently.
    if node.children.size == 0
      result += end_empty(node.tag)
      return result
    end

    result += nl(">")

    node.children.each{|kid| result += render_node(kid, depth + 1) }

    result += nl("#{indent(depth)}</#{node.tag}>")
  end

  private

  def make_tag_node(name, attributes)
    check("Tag name #{name} contains invalid character!") { valid_word? name }

    tag_attribs = {}
    attributes.each_pair do |key, value|
      key = key.to_s.gsub(/_/, '-');
      check("Invalid attribute name: #{key}") { valid_word? key }
      check("Invalid type for attribute value '#{value}': #{value.class}") {
        value.is_a?(Numeric) || value.is_a?(String) || value.is_a?(Symbol)
      }
      tag_attribs[key] = value.to_s
    end

    return Node.new(name, tag_attribs, [])
  end


  def launder(str)
    escapes = {
      "&" => "amp",
      "<" => "lt",
      ">" => "gt",
      '"' => "quot",
      "'" => "apos"
    }

    return str.gsub(/([#{escapes.keys.join}])/) {|c| "&#{escapes[c]};"}
  end

  # Add a node or text at the current context
  def addchild(nodeOrText)
    @current.children.push nodeOrText
  end

  def valid_word?(word)
    return word =~ /^[-[:word:]]+$/
  end

  def with_current(node, &kidblock)
    oldcurr = @current
    @current = node
    kidblock.call
  ensure
    @current = oldcurr
  end

  protected

  def indent(depth)
    return "    "*depth if @pretty
    return ""
  end

  def nl(str = "")
    return "#{str}\n" if @pretty
    return str
  end

end


# Class to build HTML; like XmlBuilder but ensures that only known
# html tags are used.  Also handles self-closing tags correctly.
class HtmlBuilder < XmlBuilder
  SELF_CLOSING_TAGS = %w{
area base br col embed iframe hr img input link meta param source
track wbr command keygen menuitem
}.to_set

  REGULAR_TAGS = %w{a abbr address article aside audio b bdi bdo
blockquote body button canvas caption cite code colgroup data datalist
dd del details dfn dialog div dl dt em fieldset figure footer form h1
h2 h3 h4 h5 h6 head header hgroup html i ins kbd label legend
li main map mark menu meter nav noscript object ol optgroup
option output p pre progress q rb rp rt rtc ruby s samp script section
select small span strong style sub summary sup table tbody td template
textarea tfoot th thead time title tr u ul var video }.to_set
  # Note: excludes 'html' because that's implied.

  TAGS = SELF_CLOSING_TAGS + REGULAR_TAGS

  def initialize(properties = {}, **settings)
    super('html', properties, **settings)
  end

  def render
    return "<!DOCTYPE html>\n" + super
  end

  def tag(name, **attributes, &kidblock)
    check("Unknown tag: #{name}") { TAGS.include?(name) }
    super
  end

  protected

  def end_empty(tag)
    return nl(">") if SELF_CLOSING_TAGS.include?(tag)
    return nl("></#{tag}>")
  end
end


# Evaluate block as a DSL where there is a method for each html tag.
def html(properties = {}, **settings, &block)
  context = Object.new
  builder = HtmlBuilder.new(properties, **settings)

  context.instance_exec() {
    # Tags with blocks
    HtmlBuilder::REGULAR_TAGS.each{|tag|
      nm = tag  # local reference; owned by this context
      context.define_singleton_method(nm) { |text = nil, **attr, &kidblock|
        builder.tag(nm, **attr) {
          builder.text(text.to_s) if text
          kidblock.call if kidblock
        }
      }
    }

    # Self-closing tags (i.e. no children)
    HtmlBuilder::SELF_CLOSING_TAGS.each{|tag|
      nm = tag  # local reference; owned by this context
      context.define_singleton_method(nm) {|text = nil, **attr, &illegalKidBlock|
        assert("Tag #{nm} is not allowed to have child nodes.") {
          text.nil? && illegalKidBlock.nil?
        }
        builder.tag(nm, **attr)
      }
    }

    # text function (i.e. just text)
    context.define_singleton_method(:text) { |txt| builder.text(txt) }

  }

  context.instance_exec(&block)

  return builder.render
end


# Convenience function: create a minimal html document with given
# title and make the block generate the <body/> section only.
def html_body(title, **args, &block)
  return html(**args) {
    head {
      title {
        text(title)
      }
    }
    body {
      # &block was not defined inside of the block passed to html() so
      # it needs to be explicitly evaluated in this context.
      self.instance_exec(&block)
    }
  }
end
