
# Class to provide an empty context for DSLs and the like.
#
# All methods have been (notionally) removed from the interface (with
# some exceptions, all of which now begin with an underscore ('_')).
# Instead, method calls will trigger the block argument given to
# 'new'.  (This uses 'method_missing()'.)


class EvalContext < BasicObject
  # Initializer; block is evaluated by method_missing in the caller's
  # context.  Use this to hook custom behaviour.
  #
  # 'mmbody' should take 3 arguments:
  #
  #     symbol  - the name of the method called
  #     args    - array of method arguments
  #     block   - nil or the block argument
  #     
  def initialize(&mmbody)
    _singleton_class.define_method(:method_missing) {
      | *args, &block |
      symbol = args.shift
      
      # If the user block invokes method_missing directly, it will
      # behave like an undefined method.
      symbol = :method_missing unless symbol
      
      mmbody.call(symbol, args, block)
    }

    # Now override this method to also behave like an undefined class.
    _singleton_class.define_method(:initialize) {
      |*args, &block|
      method_missing(:initialize, *args, &block)
    }
  end

  # Retrieve the singleton class.
  def _singleton_class
    class << self
      return self
    end
  end

  # Rename instance_exec and instance_eval to have leading
  # underscores.  This is so that the originals behave like undefined
  # methods and so will call the method_missing hook when used.
  alias _instance_exec instance_exec
  def instance_exec(*args, &block)
    method_missing(:instance_exec, args, &block)
  end

  alias _instance_eval instance_eval
  def instance_eval(*args, &block)
    method_missing(:instance_eval, args, &block)
  end
end
