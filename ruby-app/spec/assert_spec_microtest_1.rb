
require 'assert'

# Test of 'spec'; should fail if run standalone but pass if imported

class Fail
  def nope() return false; end
  def yup() return true; end
  def throw() raise RuntimeError.new("whoah!!!!"); end
end


spec("main test; will fail") {
  f = Fail.new
  assert("MINISPEC FAILURE"){ f.nope }
}

puts "POST_SPEC"
