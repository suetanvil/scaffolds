# coding: utf-8

require 'spec_helper'

require_relative '../lib/assert.rb'

class Bloop < RuntimeError; end
class Blarp < Bloop; end

describe Assertions do

  it "throws AssertFailure and CheckFailure on false results" do
    expect { assert("bad") { false } } .to raise_error(AssertFailure, "bad")
    expect { assert_dbg("bad") { false } } .to raise_error(AssertFailure, "bad")
    expect { check("not good") { false } } .to raise_error(CheckFailure, "not good")
  end

  it "does nothing on true results" do
    expect( assert("good") { true } )     .to be nil
    expect( assert_dbg("good") { true } ) .to be nil
    expect( check("very good") { true } ) .to be nil
  end

  it "evaluates the block" do
    count = 0
    expect( assert("good") { count += 1 } )     .to be nil
    expect( assert_dbg("good") { count += 1 } ) .to be nil
    expect( check("very good") { count += 1 } ) .to be nil

    expect(count) .to be 3
  end
  
  it "'s.assert_dbg does nothing after .no_debug! is called" do
    Assertions.no_debug!
    
    count = 0
    expect( assert("good") { count += 1 } )     .to be nil
    expect( assert_dbg("good") { count += 1 } ) .to be nil
    expect( check("very good") { count += 1 } ) .to be nil

    expect(count) .to be 2

    expect { assert("bad") { false } } .to raise_error(AssertFailure, "bad")
    expect( assert_dbg("bad") { false } ) .to be nil
    expect { check("not good") { false } } .to raise_error(CheckFailure, "not good")
  end

  it "can assert that an exception was thrown by a block" do
    # Expected exception
    assert_thrown("thrown", Bloop) { raise Bloop.new }

    # Subclass of asserted exception type
    assert_thrown("thrown", Bloop) { raise Blarp.new }

    # No exception
    expect {
      assert_thrown("thrown", Bloop, Blarp) { }
    } .to raise_error(AssertFailure)

    # Not the expected exception
    expect {
      # Not the expected assertion type
      assert_thrown("thrown", Blarp) { throw Bloop.new }
    } .to raise_error(AssertFailure)
  end

  it "does low-friction unit tests in source files" do

    # In-file tests that fail
    x = `#{RbConfig.ruby} -I lib/ spec/assert_spec_microtest_1.rb`
    expect( $?.exitstatus ) .not_to be 0
    expect( x ) .to match( /MINISPEC FAILURE/ )
    expect( x ) .not_to match( /POST_SPEC/ )

    # But if included, it doesn't run...
    x = `#{RbConfig.ruby} -I lib/ -I spec/ spec/assert_spec_microtest_2.rb`
    expect( $?.exitstatus ) .to be 0
    expect( x ) .to match( /POST_SPEC/ )

    # But the new mainline's spec *will* run
    x = `#{RbConfig.ruby} -I lib/ -I spec/ spec/assert_spec_microtest_3.rb`
    expect( $?.exitstatus ) .to be 0
    expect( x ) .to match( /POST_SPEC/s )
    expect( x ) .to match( /TESTS_RUN/s )
  end
  
end
