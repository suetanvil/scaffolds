
# Tests for *some* of util.rb; this is nowhere near complete.

require_relative '../lib/util.rb'

describe PipeCleaner do
  it "wraps a pipe" do
    data = "All work and no play makes Jack a dull boy.\n" * 200

    readPipe, writePipe = IO.pipe
    pid = Process.fork do
      writePipe.close()
      read_data = readPipe.read
      exit read_data == data ? 0 : 1
    end

    if Process.pid != pid
      pc = PipeCleaner.new(writePipe)
      pc.puts data

      writePipe.close
      pid, status = Process.wait2(pid)

      expect(status.to_i) .to be 0
    end
  end

  it "gracefully handles a closed pipe" do
    data = ["All work and no play makes Jack a dull boy.\n"] * 200

    readPipe, writePipe = IO.pipe
    pid = Process.fork do
      writePipe.close()

      read_data = []
      10.times {
        read_data.push readPipe.readline
      }

      exit read_data == data[0..9] ? 0 : 1
    end

    if Process.pid != pid
      pc = PipeCleaner.new(writePipe)
      data.each{|line| pc.puts line}

      writePipe.close
      pid, status = Process.wait2(pid)

      expect(status.to_i) .to be 0
    end
  end

end
