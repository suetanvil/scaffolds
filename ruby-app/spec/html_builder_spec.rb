require 'spec_helper'

require_relative '../lib/html_builder.rb'

# These are kind of hacky and incomplete but they're better than
# nothing

describe XmlBuilder do
  it "builds xml" do
    b = XmlBuilder.new('html')
    b.tag 'head' do
      b.tag 'title', font: "cool", speed: 1000, color: "black&white" do
        b.text "Website!  <gratuitious_tag>yes!</>"
        b.tag 'br'
      end
    end

    r = b.render

    expect(r) .to match(/<html> \s+ <head> \s+ <title \s+ font=/x)
    expect(r) .to match(/"black&amp;white"/)
    expect(r) .to match(/&lt;gratuitious_tag&gt;yes!&lt;\/&gt;/)
    expect(r) .not_to match(/<gratuitious_tag>yes!<\/>/)
    expect(r) .not_to match(/"black&white"/)
    expect(r) .to match(/<\/title> \s* <\/head> \s* <\/html> \s*$/x)
  end

  it "builds fake xhtml" do
    b = XmlBuilder.new('xhtml')
    b.tag 'blahtml' do
      b.tag 'head' do
        b.tag 'title', font: "cool", speed: 1000, color: "black&white" do
          b.text "Website!  <gratuitious_tag>yes!</>"
          b.tag 'br'
        end
      end
    end
    b.tag 'xxxhtml' do
      b.tag 'head' do
        b.tag 'title', font: "cool", speed: 1000, color: "black&white" do
          b.text "Website!  <gratuitious_tag>yes!</>"
          b.tag 'br'
        end
      end
    end

    r = b.render
    
    expect(r) .to match(/<br\/>/)
    expect(r) .to match(/^<xhtml>\s*<blahtml>\s*<head>/)
    expect(r) .to match(/speed="1000"/)
    expect(r) .to match(/<\/blahtml>\s*<xxxhtml>/)
  end
end
  
describe HtmlBuilder do
  it "builds html in the object" do
    b = HtmlBuilder.new({manifest: 'thingy'}, pretty: true)
    b.tag 'head' do
      b.tag 'title', font: "cool", speed: 1000, color: "black&white" do
        b.text "Website!  <gratuitious_tag>yes!</>"
        b.tag 'br'
      end
    end

    r = b.render

    expect(r) .to match(/<html manifest="thingy">/)
    expect(r) .to match(/^<!DOCTYPE html>/)
    expect(r) .to match(/&lt;gratuitious_tag&gt;yes!&lt;\/&gt;/)
    expect(r) .to match(/<br>\s*<\/title>\s*<\/head>\s*<\/html>/x)
  end

  it "builds html with a function" do
    r = html({manifest: 'thingy'}, pretty: true) {
      head {
        title(font: "cool", speed: 1000, color: "black&white") {
          text "Website!  <gratuitious_tag>yes!</>"
        }
      }
      body {
        p {
          i { text "MOAR WEBS1T3!!!<<><><>" }
        }
      }
    }

    expect(r) .to match(/<html manifest="thingy">/)
    expect(r) .to match(/^<!DOCTYPE html>/)
    expect(r) .to match(/&lt;gratuitious_tag&gt;yes!&lt;\/&gt;/)
    expect(r) .to match(/<\/html>\s*$/x)
  end

  it "can deal with pure text children" do
    r = html {
      head {
        text "This is the heading."
      }
      body {
        text "this is the body."
      }
    }

    expect(r) .to match(/This is the heading./)
    expect(r) .to match(/this is the body./)
  end

  it "has a convenient version that does the heading for you." do
    r = html_body("WeBSIte!!!!!") {
      p {
        h6 { text "This is some html, maaaaahn!" }
      }
    }
    expect(r) .to match(/<head>\s*<title>\s*WeBSIte!!!!!/)
    expect(r) .to match(/<body>\s*<p>\s*<h6>\s*This is some/)
  end
end
