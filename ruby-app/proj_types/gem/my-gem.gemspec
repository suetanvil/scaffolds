
# Skeleton gemspec

Gem::Specification.new do |s|
  s.name        = 'my-gem'
  s.version     = '1.0.0'
  s.date        = '2021-03-16'
  s.summary     = "DESCRIPTION GOES HERE"
  s.description = <<-EOF
    LONG DESCRIPTION GOES HERE
EOF
  s.authors     = ["Firstname Lastname"]
  s.email       = 'me@myhost'

  # I'm just going to add everything so that if you've got the gem,
  # you've also got the source distribution.  Yay! Open source!
  s.files       = ["README.md", "LICENSE.txt", "my-gem.gemspec",
                   "Rakefile", ".yardopts"] +
                  Dir.glob('doc/**/*') +
                  Dir.glob('{spec,lib}/*.rb')

  s.required_ruby_version = '>= 2.2.0'
  s.requirements << "blah blah blah"

  s.add_development_dependency "rspec", '~> 3.7', '>= 3.7.0'
  s.add_development_dependency "yard", '~> 0.9.12', '>= 0.9.12'
  
  s.homepage    = 'https://gitlab.com/me/my-gem'
  s.license     = 'AGPL'
end
