# Skeleton projects for various languages.

This is my personal collection of project starters for various
languages.  I normally just copy the files I need for my project and
modify them as necessary, occasionally migrating useful stuff back
into this repo.

It's mainly on Gitlab so that I can snag a copy when I need it without
having to log in to my personal repo but feel free to use it if you
want.  I make no guarantees that there is anything here that is useful
to you.


