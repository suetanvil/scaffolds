#!/bin/bash

# YOUR COMMENT HERE

set -e

# Absolute path to the directory containing this script
SCRIPT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"

#
# Helper functions
#

# Print an error message to stderr and exit with status 1
function die() {
    echo >&2 $*
    exit 1
}

# Test if the given executable is in the path
function has() {
    if `type -t "$1" >/dev/null 2>&1 ` ; then
        return 0
    fi

    return 1
}

# Argument parser:
#
#   parse_args "<arglist>" "$@"
#
#       <arglist> - space-separated argument names composed entirely
#       of "word" characters.  Trailing '=' means it takes an argument.
#
#       "$@" - list of actual arguments; typically the real "$@".
#
#   Output:
#
#       - FLAG_<arg-name> is 1 if the argument was given.
#
#       - ARG_<arg-name> holds its argument if it takes one.
#
#       - ARG_<number> holds each positional argument in 
#       
# use case: parse_args "help name= verbose" $0 "$@"
function parse_args() {
    local arglist=$1
    shift;

    local nl=$'\n'
    local default_help=

    local flagnames=""
    local cases=""
    for arg in $arglist; do
        argbase=${arg%=}

        flagnames+=" [--$argbase"

        case="--$argbase) FLAG_${argbase}=1"
        if [ "$arg" != "$argbase" ]; then
            case+="; shift; ARG_${argbase}=\"\$1\""
            flagnames+=" <arg>"
        fi
        case+=" ;;"

        cases+="$case "

        flagnames+="]"
    done

    # Handle --help unless overridden
    cases+="--help) default_help=1 ;; "

    # Handle invalid flags:
    cases+="-?*) die \"Unknown option: \$1\" ;; "

    # Now, parse the arguments.
    while [ -n "$1" ]; do
        if [ "$1" = "--" ]; then
            shift
            break
        fi
        eval "case \"\$1\" in ${cases} esac"
        shift
    done

    # Now, put the positional arrays in their own variables.  (Bash
    # arrays may be a better option in a few cases, but not most.)
    local index=1
    while [ -n "$1" ]; do
        eval "ARG_${index}='$1'"
        shift
        index=$((index + 1))
    done

    # Print a help message if --help was given and not specified in
    # arglist.
    if [ -n "$default_help" ]; then
        echo "USAGE: $0 $flagnames [args...]"
        exit 0
    fi
}


# Example code; delete and put your own stuff here
parse_args "verbose mode=" "$@"
set | grep '^\(ARG\|FLAG\)_' || true
echo ARGS:  "$@"

has ls          && echo "found ls"
has sdfasfa     && echo "no sdfasfa"
