
// Source: https://stackoverflow.com/questions/22501642/display-popup-in-win32-console-application

#include <windows.h> 
#include <stdio.h>


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR pCmdLine, int nCmdShow) {
    MessageBox(NULL, "Hello, World!", "Greeting", MB_OK);
    printf("Console message!\n");
    return 0; 
} 
